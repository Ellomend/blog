// import something here
import gql from 'graphql-tag'

export const stuff = {
  // register
  CREATE_USER_MUTATION: gql`
      mutation RegisterMutation($creds: CreateUserInput!) {
          createUser(
              input : $creds,
          ) {
              token
              changedUser {
                  id
                  username
              }
          }
      }
  `,
  // login
  LOGIN_MUTATION: gql`
      mutation LoginMutation($creds: LoginUserInput!) {
          loginUser(
              input : $creds,
          ) {
              token
              user {
                  id
                  username
              }
          }
      }
  `,
  GET_TASKS: gql`query getTasks {
      viewer {
          allTasks {
              edges {
                  node {
                      id
                      name
                      desc
                  }
              }
          }
      }
  }`,
  // create task
  CREATE_TASK: gql`
      mutation createTaskMutation($task: CreateTaskInput!) {
          createTask(
              input : $task,
          ) {
              changedTask {
                  id
                  name
                  desc
              }
          }
      }
  `,
  // create task
  DELETE_TASK: gql`
      mutation deleteTaskMutation($id: DeleteTaskInput!) {
          deleteTask(
              input : $id,
          ) {
              changedTask {
                  name
              }
          }
      }
  `
}

// leave the export, even if you don't use it
export default ({app, router, Vue}) => {
  // something to do
  Vue.prototype.$graphql = stuff
}

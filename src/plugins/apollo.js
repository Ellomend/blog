// import something here
import {ApolloClient} from 'apollo-client'
import {HttpLink} from 'apollo-link-http'
import {InMemoryCache} from 'apollo-cache-inmemory'
import { setContext } from 'apollo-link-context'
import VueApollo from 'vue-apollo'
import { user } from './user'
// leave the export, even if you don't use it
export default ({app, router, Vue}) => {
  const httpLink = new HttpLink({
    // You should use an absolute URL here
    uri: 'https://us-west-2.api.scaphold.io/graphql/parsimonious-nut'
  })
  const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = user.getTokenFromStorage()
    // return the headers to the context so httpLink can read them
    if (token) {
      headers.authorization = token
    }
    return {
      headers: {
        ...headers
      }
    }
  })

  // Create the apollo client
  const apolloClient = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
    connectToDevTools: true
  })

  // Install the vue plugin
  const apolloProvider = new VueApollo({
    defaultClient: apolloClient
  })
  Vue.use(VueApollo)
  app.provide = apolloProvider.provide()
  Vue.prototype.$client = apolloProvider
  console.log('provide', apolloProvider.provide())
}

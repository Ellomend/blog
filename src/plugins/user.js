import { LocalStorage } from 'quasar'
export const user = {
  user: {
    token: ''
  },
  saveTokenToStorage (token) {
    this.user.token = 'Bearer : ' + token
    LocalStorage.set('token', this.user.token)
  },
  getTokenFromStorage () {
    console.log('getting token')
    console.log(LocalStorage)
    LocalStorage.get.item('token')
  },
  clearToken () {
    LocalStorage.clear()
  },
  checkToken () {
    return !!LocalStorage.get.item('token')
  }
}

export default ({app, router, Vue}) => {
  user.app = app
  Vue.prototype.$user = user
  // auth mixins
  Vue.mixin({
    computed: {
      isLoggedIn () {
        return !!this.$store.state.user.user
      }
    },
    methods: {
      logout () {
        console.log('logout')
        this.$store.commit('user/logout')
        this.$user.clearToken()
        this.$router.push({ path: '/' })
      },
      /**
       *commit mutation and save to ls
       * @param data
       */
      login (data) {
        this.$store.commit('user/login', data.data.loginUser.user)
        console.log('saving token', data.data.loginUser.token)
        this.$user.saveTokenToStorage(data.data.loginUser.token)
      }
    }
  })
}

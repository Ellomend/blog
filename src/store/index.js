import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import example from './module-example'
import user from './user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    example,
    user
  },
  plugins: [createPersistedState({
    paths: ['user']
  })]
})

export default store

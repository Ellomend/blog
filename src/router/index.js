import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
import routes from './routes'

Vue.use(VueRouter)

const Router = new VueRouter({
  /*
   * NOTE! Change Vue Router mode from quasar.conf.js -> build -> vueRouterMode
   *
   * When going with "history" mode, please also make sure "build.publicPath"
   * is set to something other than an empty string.
   * Example: '/' instead of ''
   */

  // Leave as is and change from quasar.conf.js instead!
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE,
  scrollBehavior: () => ({ y: 0 }),
  routes
})
Router.beforeEach((to, from, next) => {
  // if user logged in redirect from register and login to home
  // console.log('store')
  // console.log(store)
  // console.log(store.state)
  console.log('to')
  console.log(to)
  // console.log('from')
  // console.log(from)
  if (to.meta && to.meta.guestOnly === true && store.getters['user/getUser'] !== null) {
    // console.log('state user')
    // console.log(store.getters['user/getUser'])
    console.log('redirect user to home')
    next({ path: '/' })
  }
  if (to.meta && to.meta.userOnly === true && !store.state.user) {
    console.log('redirect guest to login')
    next({ path: '/login' })
  }
  next()
})
export default Router

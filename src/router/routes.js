
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '', name: 'home', meta: {guest: true}, component: () => import('pages/index') },
      {
        path: '/login',
        name: 'login',
        meta: {
          guestOnly: true
        },
        component: () => import('pages/LoginPage')
      },
      {
        path: '/register',
        name: 'register',
        meta: {
          guestOnly: true
        },
        component: () => import('pages/RegisterPage')
      },
      {
        path: '/admin',
        name: 'admin',
        meta: {
          userOnly: true
        },
        component: () => import('pages/AdminPage') }
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
